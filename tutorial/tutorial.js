var minimize = true;
var maximize = false;

$(function() {
  $("#nav-link-tutorial").addClass("active");
  $("#console-text-input").keypress(function(e) {
    if (e.which === 13) {
      let code = $("#console-text-input").val();
      $(`<div>>&nbsp;${code}</div>`).appendTo("#console-display");

      run(code);

      $("#console-text-input").val("");
    }
  });

  $("#console-textarea-input").keypress(function(e) {
    if (e.which === 10) {
      let code = $("#console-textarea-input").val();
      $(`<div>>&nbsp;${code}</div>`).appendTo("#console-display");

      run(code);

      $("#console-textarea-input").val("");
    }
  });

  $("#minimize").click(fn_minimize);
  $("#maximize").click(fn_maximize);

  $("<div>Nucleoid - Declarative Runtime Environment</div>").appendTo("#console-display");
  $("<div>https://nucleoid.org/</div>").appendTo("#console-display");
  $("<br/>").appendTo("#console-display");

  $("#console-text-input").focus();
  $("#console-textarea").hide();

  setTimeout(function() {
    fn_minimize();
  }, 3000);
});

function run(code) {
  let details;

  details = nucleoid.run(code, true, true);
  let result = details.result;
  let time = details.time;

  if (result !== undefined) {
    if (details.error) {
      $(`<div>${JSON.parse(result)}</div>`).appendTo("#console-display");
    } else {
      $(`<div>${result}</div>`).appendTo("#console-display");
    }
  }

  $("#console-display").scrollTop($("#console-display").prop("scrollHeight"));
}

function fn_maximize() {
  if (!maximize) {
    $("#console").animate({ height: 650, width: 650 });
    $("#console-display").animate({ height: 420, width: 650 });

    $("#console-text").hide();
    $("#console-textarea").show();
    $("#console-textarea-input").focus();
    $("#console-shortcut").show();

    maximize = true;
    minimize = false;
  } else {
    $("#console").animate({ height: 400, width: 400 });
    $("#console-display").animate({ height: 350, width: 400 });

    $("#console-text").show();
    $("#console-textarea").hide();
    $("#console-text-input").focus();
    $("#console-shortcut").hide();

    maximize = false;
    minimize = false;
  }
}

function fn_minimize() {
  if (minimize) {
    $("#console").animate({ height: 400, width: 400 });
    $("#console-display").animate({ height: 350, width: 400 });
    minimize = false;
    maximize = false;

    $("#console-text").show();
    $("#console-textarea").hide();
    $("#console-text-input").focus();
  } else {
    $("#console").animate({ height: 25, width: 400 });
    minimize = true;
    maximize = false;
  }

  $("#console-shortcut").hide();
}
