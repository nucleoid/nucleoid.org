<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Nucleoid - Tutorial</title>
    <?php include "../head.php" ?>
    <link rel="stylesheet" href="tutorial.css" />
    <link rel="stylesheet" href="bootstrap-toc.min.css">
    <script src="tutorial.js"></script>
    <script src="bundle.js"></script>
    <script src="bootstrap-toc.min.js"></script>
    <script>
    </script>
  </head>
  <body data-spy="scroll" data-target="#toc">
    <nav class="d-none d-lg-block" id="toc" data-spy="affix" data-toggle="toc" style="position: fixed; top: 100px; left: 5px"></nav>

    <?php include "../menu.php" ?>
    <div class="panel container">
      <div class="row">
        <div class="d-none d-lg-block col-lg-1"></div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
          <?php include "tutorial.html" ?>
        </div>
      </div>
    </div>
    <div id="console" class="console d-none d-lg-block">
      <div class="console-top text-right">
        <span id="minimize">_</span>
        <span id="maximize">&squ;&nbsp;</span>
      </div>
      <div id="console-display" class="console-display"></div>
      <div id="console-text">>&nbsp;<input id="console-text-input" class="console-input" type="text" /></div>
      <div id="console-textarea">
        <textarea id="console-textarea-input"></textarea>
        <div id="console-shortcut" style="position: absolute; right: 5px; bottom: 0px; color: gray">
          Ctrl + Enter
        </div>
      </div>
    </div>
    <?php include "../report.php" ?>
    <?php include "../footer.php" ?>
  </body>
</html>
