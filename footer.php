<footer class="page-footer" style="background-color: #353e48; color: #a9a9a9; font-size: 15px">
  <div class="footer-copyright text-center py-2">
    <?= date("l") . " " . ceil((ceil((time() - strtotime("1970-01-01")) / (60 * 60 * 24)) + 3) / 7); ?>
    <br />
    <span style="display: inline-block; transform: scale(-1, 1);">©</span> 2019-2020 Nucleoid
  </div>
</footer>
