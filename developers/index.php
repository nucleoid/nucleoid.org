<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Nucleoid - Developers</title>
    <?php include "../head.php" ?>
  </head>
  <body>
    <?php include "../menu.php" ?>
    <div class="panel container">
      <div class="row">
        <div class="col-12">
          <h3>Files</h3>
          <iframe id="drive" src="https://drive.google.com/embeddedfolderview?id=1Jj4MQhk1PtfQi8iHE4bvF2ABCI6wP3rG#grid" width="1140" height="698" frameborder="0"></iframe>
        </div>
      </div>
    </div>
    <?php include "../report.php" ?>
    <?php include "../footer.php" ?>
  </body>
</html>
