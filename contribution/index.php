<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Nucleoid - Contribution</title>
    <?php include "../head.php" ?>
    <link rel="stylesheet" href="contribution.css" />
    <script src="contribution.js"></script>
  </head>
  <body>
    <?php include "../menu.php" ?>
    <div class="panel container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <div class="box">
            <a class="box" href="https://gitlab.com/nucleoid/nucleoid"><img src="/media/gitlab.png" width="50px"/></a> Contribute at Gitlab
          </div>
          <div class="box">
            <a class="box" href="https://trello.com/b/TZ73H1Fk/nucleoid"><img src="/media/trello.png" width="50px"/></a> Project Conversations
          </div>
          <div class="box">
            <a class="box" href="http://git.nucleoid.org"><img src="/media/git.png" width="50px"/></a> Download repository
          </div>
          <div class="box">
            <a class="box" href="https://dev.to/nucleoid"><img src="/media/dev.to.png" width="50px"/></a> Go to articles
          </div>
          <div class="box">
            <a class="box" href="https://www.reddit.com/r/nucleoid/"><img src="/media/reddit.png" width="50px"/></a> Join conversations at Reddit
          </div>
          <div class="box">
            <a class="box" href="https://stackoverflow.com/questions/tagged/nucleoid"><img src="/media/stackoverflow.png" width="50px"/></a> Ask a quesion at Stack Overflow
          </div>
          <div class="box">
            <a class="box" href="/developers/"><img src="/media/icon.png" width="50px"/></a> Go to developers page
          </div>
          <div class="box">
            <a class="box" href="#"><img src="/media/rss.png" width="50px"/></a> Receive RSS feeds
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <h3>Announcements</h3>
          <div><i>11/8/2020</i> - Nucleoid IDE's first release <a href="https://gitlab.com/nucleoid/ide">Go to Repo</a></div>
          <hr />
          <div><i>10/30/2020</i> - 1.1 release is out! Download <a href="http://nucleoid.org/download/">here</a></div>
          <hr />
          <div><i>12/23/2019</i> - Alpha release is out there! Give a try <a href="http://nucleoid.org/download/">here</a></div>
          <hr />
          <div><i>12/16/2019</i> - Migration to GitLab is completed, See the new home <a href="https://gitlab.com/nucleoid/nucleoid">https://gitlab.com/nucleoid/nucleoid</a></div>
          <hr />
          <div><i>04/12/2019</i> - Defensive publication of declarative runtime is published. Read at <a href="https://medium.com/@canmingir/d48bbf52c94e">Medium</a></div>
        </div>
        <div class="d-none d-lg-block col-lg-4 text-right" style="font-size: 30px">
          <div class="rainbow-box">
            <div>Power</div>
            <div>of</div>
            <div>Declarative</div>
            <div>Programming</div>
            <div>Welcomes</div>
            <div>You</div>
            <div>with</div>
            <div><img src="/media/love.png" width="40px" /></div>
          </div>
        </div>
      </div>
    </div>
    <?php include "../report.php" ?>
    <?php include "../footer.php" ?>
  </body>
</html>
