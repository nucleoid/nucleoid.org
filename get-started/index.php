<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Nucleoid - Get Started</title>
    <?php include "../head.php" ?>
    <link rel="stylesheet" href="get-started.css" />
    <script src="get-started.js"></script>
  </head>
  <body>
    <?php include "../menu.php" ?>
    <div class="panel container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <h4>Download</h4>
          <pre><code>wget https://nucleoid.org/download/ -O nucleoid.zip</code></pre>
          <br />
          <h4>Clone</h4>
          <pre><code class="bash">git clone https://gitlab.com/nucleoid/nucleoid.git</code></pre>
          <br />
          <h4>Install</h4>
          <pre><code>sudo apt-add-repository ppa:nucleoid/nucleoid
sudo apt install nucleoid
</code></pre>
          <h4>Docker</h4>
          <pre><code>docker run -d -p 80:80 nucleoid/nucleoid</code></pre>
          <br />
          <hr />
          <br />
          <div class="text-center">
            <h4>Get source codes at</h4>
            <a class="box" href="https://gitlab.com/nucleoid/nucleoid"><img src="/media/gitlab.png" width="75px"/></a>
            <a class="box" href="http://git.nucleoid.org"><img src="/media/git.png" width="75px"/></a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <h4>Hello World</h4>
          Open the terminal on your browser
          <pre><code class="javascript">> a = 1
> b = a * 2
> b
2
> a = 2
> b
4
</code></pre>
          <br />
          <div class="text-center">
            <img src="/media/plant.png" width="300" />
          </div>
        </div>
      </div>
    </div>
    <?php include "../report.php" ?>
    <?php include "../footer.php" ?>
  </body>
</html>
