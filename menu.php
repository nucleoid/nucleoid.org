<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/">
    <font color="#28a745">Nucleoid</font>
  </a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a id="nav-link-home" class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
        <a id="nav-link-get-started" class="nav-link" href="/get-started/">Get Started</a>
      </li>
      <li class="nav-item">
        <a id="nav-link-tutorial" class="nav-link" href="/tutorial/">Tutorial</a>
      </li>
      <li class="nav-item">
        <a id="nav-link-specifications" class="nav-link" href="/specifications/">Specifications</a>
      </li>
      <li class="nav-item">
        <a id="nav-link-contribution" class="nav-link" href="/contribution/">Contribution</a>
      </li>
    </ul>
  </div>
</nav>
