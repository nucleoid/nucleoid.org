$(function() {
  $("p").each(function(index) {
    let text = $(this).text();
    $(this).text(index + 1 + ". " + text);
  });

  $("#nav-link-specifications").addClass("active");
});
