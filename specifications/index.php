<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Nucleoid - Specifications</title>
    <?php include "../head.php" ?>
    <link rel="stylesheet" href="specifications.css" />
    <script src="specifications.js"></script>
  </head>
  <body>
    <?php include "../menu.php" ?>
    <div class="panel container">
      <div class="row">
        <div class="col-12">
          <?php include "specifications.html" ?>
        </div>
      </div>
    </div>
    <?php include "../report.php" ?>
    <?php include "../footer.php" ?>
  </body>
</html>
