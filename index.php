<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Nucleoid - Declarative Runtime Environment</title>
    <?php include "head.php" ?>
    <style>
      img.icon {
        margin: 10px;
      }

      pre {
        border-radius: 10px;
      }
    </style>
    <script src="index.js"></script>
  </head>
  <body>
    <?php include "menu.php" ?>
    <div id="prompt-container" style="height: 500px">
      <div id="vertical-text" class="d-none d-lg-block" style="font-size: 50px; writing-mode: vertical-rl; text-orientation: sideways; position:absolute; top:90px; left: 0px;">Power&nbsp;of&nbsp;Declarative</div>
      <div class="container">
        <div class="row">
          <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
          <div class="d-none d-lg-block col-lg-5">
            <div style="margin-top: 150px">
              <div style="max-width: 300px; color: #101010">
                <h2 id="prompt-title"></h2>
                <h5><i class="text-dark" id="prompt-transcription"></i></h5>
                <p id="prompt-description" style="color: #202020"></p>
                <a href="tutorial" style="color: #000000"><i>Learn more..</i></a>
              </div>
            </div>
          </div>
          <div class="col-xs-11 col-sm-11 col-md-11 col-lg-6 text-center">
            <img id="prompt-image" style="height: auto; max-width: 100%;" />
          </div>
        </div>
      </div>
    </div>
    <br />
    <div>
      <div class="d-none d-lg-block" style="font-size: 50px; color:#343a40; writing-mode: vertical-rl; text-orientation: sideways;position:absolute; top:570px; left: 0px;">Programming&nbsp;Welcomes&nbsp;You</div>
      <div class="container">
        <br />
        <div class="row">
          <div class="col-10 offset-1 col-lg-6 offset-lg-3 text-center">
            <p class="lead">Nucleoid is a runtime environment for declarative programming in vanilla JavaScript syntax and run as a programming runtime and datastore.</p>
          </div>
        </div>
        <br />
        <br />
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
            <a href="#control-flow-by-logic">
              <img class="icon" src="media/007-AI.png" width="70px" height="70px" />
            </a>
            <h3>Control Flow by Logic</h3>
            Control flow is defined based on formal logic instead of explicit steps; it enables writing codes with building blocks with less code lines.
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
            <a href="#persistency">
              <img class="icon" src="media/019-database-5.png" width="70px" height="70px" />
            </a>
            <h3>Persistency</h3>
            The programming runtime persists each statement so that it doesn't require external data storage like traditional RDBMS.
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
            <a href="#logical-integrity">
              <img class="icon" src="media/019-cube.png" width="70px" height="70px" />
            </a>
            <h3>Logical Integrity</h3>
            The runtime provides logical integrity after each transaction out-of-the-box.
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
            <img class="icon" src="media/007-transfer-1.png" width="70px" height="70px" />
            <h3>Cloud Integration</h3>
            Nucleoid dynamically accepts declarative statements instead of compiling; it is alternative to serverless model.
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
            <a href="#simplify-architecture">
              <img class="icon" src="media/030-sync-4.png" width="70px" height="70px" />
            </a>
            <h3>Simplify Architecture</h3>
            The runtime doesn't require database or cache server, which reduces number of elements have to be maintained.
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
            <img class="icon" src="media/031-robotic-hand.png" width="70px" height="70px" />
            <h3>Machine Learning</h3>
            Redefining the definition of data offers different perspective to statistical learning.
          </div>
        </div>
        <br />
        <br />
        <br />
        <div class="row">
          <div class="col-12 text-center">
            <a class="box" href="https://gitlab.com/nucleoid/nucleoid"><img src="/media/gitlab.png" width="50px"/></a>
            <a class="box" href="https://trello.com/b/TZ73H1Fk/nucleoid"><img src="/media/trello.png" width="50px"/></a>
            <a class="box" href="https://dev.to/nucleoid"><img src="/media/dev.to.png" width="50px"/></a>
            <a class="box" href="https://twitter.com/NucleoidProject"><img src="/media/twitter.png" width="50px"/></a>
            <h3>Nucleoid is an open source project with <a style="color: #212529" href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></h3>
            <h4>Join the community</h4>
          </div>
        </div>
        <br />
        <br />
        <br />
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-7 col-lg-5">
            <br />
            <pre><code class="javascript">> a = 1
> b = a * 2
> b
2
> a = 2
> b
4</code></pre>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-5 col-lg-7">
            <h3>Hello world, it is me!</h3>
            The declarative runtime environment isolates a behavior definition of a program from its technical instructions and executes declarative statements, which represent logical intention without carrying any technical details. In this paradigm, there is no segregation regarding what data is or not, instead approaches how data is related with others so that any type of data including business rules can be added without requiring any additional actions such as compiling, configuring, restarting as a result of plasticity.
            <br />
            <br />
            <div class="text-center">
              <button class="btn" type="button" style="background-color: #008a7a; color: #FFFFFF" onclick="window.location.assign('/tutorial')">
                Go to Tutorial
              </button>
            </div>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h3 id="control-flow-by-logic">Control Flow by Logic</h3>
            <img src="media/024-idea.png" align="left" width="90px" style="margin: 20px" />
            <p>One of the biggest differences in declarative programming oppose to imperative programming is who manages the control flow. In IP, a programmer has full control of instructions that runs through CPU, but in DP, it is based on semantics.</p>
            <p>
              Nucleoid follows formal logic as semantics so that each statement is a part of dependency graph and the runtime builds control flow of related statements accordingly.
              <img src="media/002-AI.png" align="right" width="120px" style="margin: 20px" />
              Since control flow is built just-in-time, additional statements also alter the flow, which lets building behaviors with declarative statements. However, in IP, control flow is written within compile codes, alteration requires complete cycle.
            </p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <h3 id="logical-integrity">Logical Integrity</h3>
            <img src="media/011-formula.png" align="left" width="90px" style="margin: 20px" />
            <p>Even though modern databases provide data integrity, logical integrity still has to be part of coding, but programming languages don't provide this such a crucual feature neither. Since the runtime tracks relationships between declarative statements, it is capable of providing logical integrity after each statement.</p>
            <p>This model reduces code lines tremendously as well as data flow inside program along with side effects, in addition, prevents programmer nightmares of spaghetti codes, callback hells etc. with simplifying coding structure.</p>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-12">
            <h3 id="persistency">Persistency</h3>
            <img class="d-none d-sm-block" src="media/014-database.png" align="left" width="175px" style="margin: 40px" />
            <p>Traditional programming languages don't store object state so that require external data storage, which brings additional complexity to the system. Thanks to declarative programming, by its mechanics, it enables programming runtime to store data at the same time.</p>
            <p>Nucleoid can do everything tradition databases do with reducing complexity including advance queries, complex data structures, transactions, data integrity etc. In addition, it keeps state of object in memory so that automaically enables caching also inside the runtime.</p>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-12">
            <h3 id="simplify-architecture">Simplify Architecture</h3>
            <img class="d-none d-sm-block" src="media/027-sync-1.png" align="right" width="175px" style="margin: 40px" />
            <p>Modern systems carry polyglot databases, cache server, log server, security along with programming runtime in different variety, and each element brings own complexity. Nucleoid with declarative programming reduces number of element needed without giving up any feature with simple and clean architecture.</p>
            <p>Since declarative programming separates functional intention from its implementation details, scalable model of Nucleoid is not hardcoded at all, which brings simplicity to architecture without losing any ability including still supporting existing scalability models.</p>
          </div>
        </div>
        <br />
      </div>
    </div>
    <?php include "footer.php" ?>
    <script>
      $("#report").hide();
    </script>
    <img hidden src="media/81466873.png" />
    <img hidden src="media/104964468.png" />
    <img hidden src="media/113933331.png" />
    <img hidden src="media/223109861.png" />
    <img hidden src="media/223109896.png" />
    <img hidden src="media/84588517.png" />
  </body>
</html>
