<style>
  div.report {
    position: fixed;
    bottom: 100px;
    right: 0;
    z-index: 1;
    writing-mode: vertical-rl;
    transform: rotate(180deg);
    color: white;
    background: black;
    cursor: pointer;
  }
</style>
<div id="report" class="report d-none d-sm-block">
  <a href="https://docs.google.com/forms/d/e/1FAIpQLSelgtOS29yTXuip7SONBM3aTYkur8opS7gcWlg9d7OrHXf8Fw/viewform?usp=sf_link">&nbsp;+Report&nbsp;</a>
</div>
