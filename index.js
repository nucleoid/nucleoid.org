designs = [
  {
    color: "#008aa6",
    image: "media/81466873.png",
    vertical: "white"
  },
  {
    color: "#e9ddc5",
    image: "media/223109896.png",
    vertical: "#343a40"
  },
  {
    color: "#f2ebd8",
    image: "media/113933331.png",
    vertical: "#343a40"
  },
  {
    color: "#cfc2b0",
    image: "media/104964468.png",
    vertical: "white"
  },
  {
    color: "#ededed",
    image: "media/223109861.png",
    vertical: "#343a40"
  },

  {
    color: "#9db1b8",
    image: "media/84588517.png",
    vertical: "#343a40"
  }
];

messages = [
  {
    title: "Persistency",
    transcription: "/pəˈsɪst(ə)nsi/",
    description: "The programming runtime persists each statement so that it doesn't require external data storage."
  },
  {
    title: "Integrity",
    transcription: "/inˈteɡrədē/",
    description: "The declarative runtime provides logical integrity along with data integrity after each statement."
  },
  {
    title: "Control Flow",
    transcription: "/kənˈtrōl flō/",
    description: "Control flow is automatically built based on formal logic by the runtime according to relationships of declarative statements."
  },
  {
    title: "Data",
    transcription: "/ˈdādə/",
    description: "The declarative runtime considers everything is a piece of information and builds relationships accordingly."
  },
  {
    title: "Biomimetics",
    transcription: "/ˌbīōməˈmediks/",
    description: "The declarative runtime follows formal logic as ubiquitous language of nature."
  },
  {
    title: "Plasticity",
    transcription: "/plaˈstisədē/",
    description: "The declarative runtime accepts a statement anytime without requiring compiling or restarting."
  }
];

var current;

function load(initial) {
  let random;

  if (initial) {
    random = Math.floor(Math.random() * 3);
  } else {
    random = Math.floor(Math.random() * designs.length);
  }

  let design = designs[random];

  if (random == current) {
    return;
  } else {
    current = random;
  }

  if (initial) {
    $("#prompt-image").attr("src", design.image);
    $("#prompt-container").css("background-color", design.color);
    $("#vertical-text").css("color", design.vertical);
  } else {
    $("#prompt-image")
      .fadeOut(function() {
        $("#prompt-image").attr("src", design.image);
        $("#prompt-container").css("background-color", design.color);
        $("#vertical-text").css("color", design.vertical);
      })
      .fadeIn();
  }

  random = Math.floor(Math.random() * messages.length);
  let message = messages[random];
  $("#prompt-title").text(message.title);
  $("#prompt-transcription").text(message.transcription);
  $("#prompt-description").text(message.description);
}

$(function() {
  load(true);
  var interval = setInterval(load, 15000);
  $("#nav-link-home").addClass("active");

  $([window, document])
    .focusin(function() {
      interval = setInterval(load, 15000);
    })
    .focusout(function() {
      clearInterval(interval);
    });
});
