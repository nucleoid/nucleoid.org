<?php
switch ($_SERVER['REQUEST_URI']) {
  case '/':
    $description = "Nucleoid is a runtime environment for declarative programming in ES6 syntax and provides logical integrity, multithreading, plasticity and persistency.";
    break;
  case '/get-started/':
    $description = "Download or clone Nucleoid";
    break;
  case '/tutorial/':
    $description = "Learn declarative programming with online console";
    break;
  case '/specifications/':
    $description = "Specifications of Nucleoid";
    break;
  case '/contribution/':
    $description = "Join Contribution at Nucleoid";
    break;
  case '/developers/':
    $description = "Developers site of Nucleoid";
    break;
  case '/enterprise/':
    $description = "Get enterprise support for Nucleoid";
    break;
}
?>

<meta charset="utf-8" />
<meta name="robots" content="index,follow" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="<?= $description ?>" />

<meta property="og:title" content="Nucleoid - Declarative Runtime Environment" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://nucleoid.org/media/icon-text.png" />
<meta property="og:description" content="Nucleoid is an open source (Apache 2.0), a runtime environment that allows declarative programming written in ES6 (JavaScript) syntax." />
