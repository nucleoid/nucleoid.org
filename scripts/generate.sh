#!/bin/sh
rm -Rf /tmp/nucleoid/
git clone https://gitlab.com/nucleoid/nucleoid.git /tmp/nucleoid/
cd /tmp/nucleoid/ && npm install
mocha /tmp/nucleoid/tests/nucleoid.spec.js -R markdown > /opt/nucleoid.org/specifications/specifications.md
sed -i '1d;2d;3d;4d;5d' /opt/nucleoid.org/specifications/specifications.md
browserify /tmp/nucleoid/nucleoid.js --standalone nucleoid -o /opt/nucleoid.org/tutorial/bundle.js

markdown /opt/nucleoid.org/specifications/specifications.md > /opt/nucleoid.org/specifications/specifications.html
markdown /opt/nucleoid.org/tutorial/tutorial.md > /opt/nucleoid.org/tutorial/tutorial.html

ln -s /opt/benchmark/ /opt/nucleoid.org/benchmark
ln -s /opt/reports/ /opt/nucleoid.org/reports
